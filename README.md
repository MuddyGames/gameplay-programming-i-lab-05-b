# README #

This is LAB 05 Math Library.


### What is this repository for? ###

* Lab 05 Math Library Vector3f, Matrix3f, Vector2f and Quaternion.

### How do I get set up? ###

* Clone repository

### Cloning Repository ###
* Run GitBash and type the Follow commands into GitBash

* Check Present Working Directory `pwd`

* Change to the C drive or other drive `cd c:`

* Make a projects Directory `mkdir projects`

* Change to the projects directory by `cd projects`

* Clone the project `git clone https://MuddyGames@bitbucket.org/MuddyGames/gameplay-programming-i-lab05-b.git`

* Change to the project directory `cd projects gameplay-programming-i-lab05-b`

* List files that were downloaded `ls`

### Useful Resources ###

* [Vectors](https://www.khanacademy.org/math/algebra-home#alg-vectors)
* [Matrices](https://www.khanacademy.org/math/algebra-home#alg-matrices)
* [Interactive Quaternions](https://eater.net/quaternions/)
* [C++ <assert.h>](https://en.cppreference.com/w/cpp/error/assert)
### Who do I talk to? ###

* philip.bourke@itcarlow.ie
